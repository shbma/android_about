package com.michael.aboutme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by michael on 12.04.16.
 */
public class WorksActivity extends Activity {
    ImageView ene,ust,nik,tri,lun,fut; //изображения из списка работ
    Button bGotoMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.works);

        initViews();
        setWorks();

        setGoToMainBehaviour();
    }

    //инициализация блоков-представителей
    private void initViews(){
        ene = (ImageView) findViewById(R.id.works_ene);
        ust = (ImageView) findViewById(R.id.works_ust);
        nik = (ImageView) findViewById(R.id.works_nik);
        tri = (ImageView) findViewById(R.id.works_tri);
        lun = (ImageView) findViewById(R.id.works_lun);
        fut = (ImageView) findViewById(R.id.works_fut);

        bGotoMain = (Button) findViewById(R.id.toMain);
    }

    //наполнение изображениями списка работ
    private void setWorks(){
        ene.setImageResource(R.drawable.ene2);
        ust.setImageResource(R.drawable.ust2);
        nik.setImageResource(R.drawable.nik2);
        tri.setImageResource(R.drawable.tri2);
        lun.setImageResource(R.drawable.lun2);
        fut.setImageResource(R.drawable.fut2);
    }

    //обработчики кнопки перехода на Главную
    private void setGoToMainBehaviour(){
        bGotoMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(WorksActivity.this, MainActivity.class);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
