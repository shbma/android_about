package com.michael.aboutme;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    ImageView photo;//фотка
    String[] fio_values, fio_labels; //ФИО контент
    String[] skills; //навыки
    TextView familyLabel, familyValue, surnameLabel, surnameValue, nameLabel, nameValue; //фио разметка
    TextView skillsView;

    Button bGotoWorks;//переход в другое окно

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        setPhoto();

        getFioData();
        setFioViews();

        getSkills();
        setSkills();

        setGoToWorksBehaviour();//обработчики кнопки перехода к списку работ

    }

    //обработчики кнопки перехода к списку работ
    private void setGoToWorksBehaviour(){
        bGotoWorks.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //Intent intent = new Intent(MainActivity.this, WorksActivity.class);
                Intent intent = new Intent(getApplicationContext(), WorksActivity.class);
                startActivity(intent);
            }
        });
    }

    //инициализация блоков-представителей
    private void initViews(){
        photo = (ImageView) findViewById(R.id.main_phote);

        familyLabel = (TextView) findViewById(R.id.family_view_label);
        familyValue = (TextView) findViewById(R.id.family_view);
        surnameLabel = (TextView) findViewById(R.id.surname_view_label);
        surnameValue = (TextView) findViewById(R.id.surname_view);
        nameLabel = (TextView) findViewById(R.id.name_view_label);
        nameValue = (TextView) findViewById(R.id.name_view);
        skillsView = (TextView) findViewById(R.id.skills_view);
        bGotoWorks = (Button) findViewById(R.id.toWorks);
    }

    //вывод ФИО
    private void setFioViews(){
        familyLabel.setText(fio_labels[2]);
        familyValue.setText(fio_values[2]);
        surnameLabel.setText(fio_labels[1]);
        surnameValue.setText(fio_values[1]);
        nameLabel.setText(fio_labels[0]);
        nameValue.setText(fio_values[0]);
    }

    //подставим изображение
    private void setPhoto(){
        photo.setImageResource(R.drawable.android2_spear);
    }

    //прочитаем ФИО
    public void getFioData() {
        this.fio_values = this.getResources().getStringArray(R.array.fio_values);
        this.fio_labels = this.getResources().getStringArray(R.array.fio_labels);
    }

    //соберем навыки
    public void getSkills(){
        skills = getResources().getStringArray(R.array.skills);
    }

    //вывод навыков
    public void setSkills(){
        StringBuilder textAccumulator = new StringBuilder();

        for(int i = 0; i < skills.length; i++){
            textAccumulator.append(skills[i] + "\n");
        }
        skillsView.setText(textAccumulator.toString());
    }

}
